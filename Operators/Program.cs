﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Operators
{
    class Program
    {
        static void Main(string[] args)
        {

            Camp camp1 = new Camp(23, 44, 3, 2, 3);

            Camp camp2 = new Camp(33, 55, 4, 2, 4);

            if(camp1 > camp2)
            {
                Console.WriteLine("camp1 bigger");
            } else {
                Console.WriteLine("camp2 bigger");
            }

            Camp camp3 = camp1 + camp2;

            Console.WriteLine(camp3.ToString());


            //-------edgar
            CampEdgar campEdgar = new CampEdgar(23, 44, 3, 2, 3);
            CampEdgar.SerializeACamp("camp.xml", campEdgar);
            CampEdgar xmlCamp1 = CampEdgar.DeserializeACar("camp.xml");
            CampEdgar xmlCamp2 = CampEdgar.DeserializeACar("camp.xml");

            if(xmlCamp1 == xmlCamp2)
                Console.WriteLine("objects equal");
            else
                Console.WriteLine("objects not equal");

            if (xmlCamp1.GetHashCode() == xmlCamp2.GetHashCode())
                Console.WriteLine($"hashcodes equal : {xmlCamp1.GetHashCode()}");
            else
                Console.WriteLine($"hashcodes not equal,\nxmlCamp1: {xmlCamp1.GetHashCode()}\nxmlCamp2: {xmlCamp2.GetHashCode()}");
        }
    }
}
