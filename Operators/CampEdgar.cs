﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.ComTypes;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Operators
{
    public class CampEdgar
    {
        #region Properties
        public int Id { get; set; }
        public float Latitude { get; set; }
        public float Longitude { get; set; }
        public int NumberOfPeople { get; set; }
        public int NumberOfTents { get; set; }
        public int NumberOfFleshLights { get; set; }
        public int LastCampId { get; set; } = 0;
        #endregion

        #region Constructors
        public CampEdgar()
        {

        }
        public CampEdgar(float latitude, float longitude, int numberOfPeople, int numberOfTents, int numberOfFleshLights)
        {
            ++LastCampId;

            this.Id = LastCampId;
            this.Latitude = latitude;
            this.Longitude = longitude;
            this.NumberOfPeople = numberOfPeople;
            this.NumberOfTents = numberOfTents;
            this.NumberOfFleshLights = numberOfFleshLights;
        }
        #endregion

        #region Methods
        public static void SerializeACamp(string filename, CampEdgar camp)
        {
            XmlSerializer serializer = new XmlSerializer(camp.GetType());
            using (Stream file = new FileStream(filename, FileMode.Create))
            {
                serializer.Serialize(file, camp);
            }
        }
        public static CampEdgar DeserializeACar(string filename)
        {
            XmlSerializer myXmlSerializer = new XmlSerializer(typeof(CampEdgar));

            CampEdgar newCamp = null;

            using (Stream file = new FileStream(filename, FileMode.Open))
            {
                newCamp = (CampEdgar)myXmlSerializer.Deserialize(file);

            }
            return newCamp;
        }
        public static bool operator ==(CampEdgar x, CampEdgar y) => x.Equals(y);
        public static bool operator !=(CampEdgar x, CampEdgar y) => !(x.Equals(y));
        public static bool operator >(CampEdgar x, CampEdgar y) => x.NumberOfPeople > x.NumberOfPeople;
        public static bool operator <(CampEdgar x, CampEdgar y) => x.NumberOfPeople < x.NumberOfPeople;
        public bool Equals(object o)
        {
            if (o is CampEdgar)
            {
                CampEdgar camp = (CampEdgar)o;

                return this.Id == camp.Id;
            }
            return false;
        }
        public int GetHashCode() => this.Id;
        public static CampEdgar operator +(CampEdgar x, CampEdgar y) =>
            new CampEdgar(
                x.Latitude,
                x.Longitude,
                x.NumberOfPeople + y.NumberOfPeople,
                x.NumberOfTents + y.NumberOfTents,
                x.NumberOfFleshLights + y.NumberOfFleshLights
                );

        public override string ToString() =>
            $"Latitude: {this.Latitude}\nLongitude: {this.Longitude}\nNumberOfPeople: {this.NumberOfPeople}\nNumberOfTents: {this.NumberOfTents}\nNumberOfFleshLights: {this.NumberOfFleshLights}";
        #endregion
    }
}
