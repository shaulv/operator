using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.ComTypes;
using System.Text;
using System.Threading.Tasks;

namespace Operators
{
    class Camp
    {
        #region Properties
        readonly int id;
        public float Latitude { get; private set; }
        public float Longitude { get; private set; }
        public int NumberOfPeople { get; private set; }
        public int NumberOfTents { get; private set; }
        public int NumberOfFleshLights { get; private set; }
        static int lastCampId = 0;
        #endregion

        #region Constructors
        public Camp(float latitude, float longitude, int numberOfPeople, int numberOfTents, int numberOfFleshLights)
        {
            ++lastCampId;

            this.id = lastCampId;
            this.Latitude = latitude;
            this.Longitude = longitude;
            this.NumberOfPeople = numberOfPeople;
            this.NumberOfTents = numberOfTents;
            this.NumberOfFleshLights = numberOfFleshLights;


        }
        #endregion

        #region Methods
        public static bool operator ==(Camp x, Camp y) => x.Equals(y);
        public static bool operator !=(Camp x, Camp y) => !(x.Equals(y));
        public static bool operator >(Camp x, Camp y)
        {
            if(x != null && y != null)
                return x.NumberOfPeople > y.NumberOfPeople;
            return false;
        }
        public static bool operator <(Camp x, Camp y)
        {
             if(x != null && y != null)
                return x.NumberOfPeople < y.NumberOfPeople;
            return false;
        }
        public bool Equals(object o)
        {
            if (o is Camp)
            {
                Camp camp = (Camp)o;

                return this.id == camp.id;
            }
            return false;
        }
        public int GetHashCode() => this.id;
        public static Camp operator +(Camp x, Camp y) =>
            new Camp(
                x.Latitude,
                x.Longitude,
                x.NumberOfPeople + y.NumberOfPeople,
                x.NumberOfTents + y.NumberOfTents,
                x.NumberOfFleshLights + y.NumberOfFleshLights
                );

        public override string ToString() =>
            $"Latitude: {this.Latitude}\nLongitude: {this.Longitude}\nNumberOfPeople: {this.NumberOfPeople}\nNumberOfTents: {this.NumberOfTents}\nNumberOfFleshLights: {this.NumberOfFleshLights}";
        #endregion
    }
}
